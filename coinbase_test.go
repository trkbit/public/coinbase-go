package coinbasego

import (
	"log"
	"testing"
)

func Test_GetPrice(t *testing.T) {

	client := NewClient(ApiCred{})

	r, err := client.GetPrice("USDT-BRL")
	if err != nil {
		t.Errorf(err.Error())
	}

	// b, _ := json.Marshal(r)

	log.Println(r.Amount)
}
