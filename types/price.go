package types

import "strconv"

// {"data":{"amount":"4.961","base":"USD","currency":"BRL"}}
type Price struct {
	Amount   string `json:"amount"`
	Base     string `json:"base"`
	Currency string `json:"currency"`
}

func (s Price) DataName() string {
	return "price"
}

func (s Price) PriceFloat64() float64 {
	f, _ := strconv.ParseFloat(s.Amount, 64)
	return f
}

type ExchangeRate struct {
	Currency string            `json:"currency"`
	Rates    map[string]string `json:"rates"`
}
