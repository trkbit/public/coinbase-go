package coinbasego

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/trkbit/public/coinbase-go/types"
)

const (
	COINBASE_URL = "https://api.coinbase.com"
)

type ApiCred struct {
	ApiKey    string
	ApiSecret string
}

func (s ApiCred) Validate() error {
	if s.ApiKey == "" || s.ApiSecret == "" {
		return fmt.Errorf("coinbasego: error api cred has invalid keys")
	}
	return nil
}

type Client struct {
	baseUrl string
	client  *http.Client
	apiCred *ApiCred
}

func NewClient(tokens ApiCred) *Client {
	if err := tokens.Validate(); err != nil {
		log.Println(err)
	}

	return &Client{
		baseUrl: COINBASE_URL,
		client:  &http.Client{},
		apiCred: &tokens,
	}
}

type authMessage struct {
	Timestamp int64  `json:"-"`
	Method    string `json:"method"`
	Path      string `json:"path"`
	Body      string `json:"body"`
}

func (s Client) signMessage(authmsg authMessage) string {
	message := fmt.Sprintf("%d%s%s%s", authmsg.Timestamp, authmsg.Method, authmsg.Path, authmsg.Body)
	hmac := hmac.New(sha256.New, []byte(s.apiCred.ApiSecret))
	hmac.Write([]byte(message))
	hex := hex.EncodeToString(hmac.Sum(nil))
	return hex
}

func (s Client) Get(uri string, body io.Reader, data interface{}) error {
	url := s.baseUrl + uri
	timestamp := time.Now().Unix() / 1000

	accessSign := s.signMessage(authMessage{
		Timestamp: timestamp,
		Method:    "GET",
		Path:      uri,
		Body:      "",
	})

	req, _ := http.NewRequest("GET", url, body)

	resp, err := s.client.Do(req)
	if err != nil {
		json.NewDecoder(resp.Body).Decode(&data)
		return err
	}

	req.Header.Add("CB-ACCESS-SIGN", accessSign)
	req.Header.Add("CB-ACCESS-TIMESTAMP", fmt.Sprintf("%d", timestamp))
	req.Header.Add("CB-ACCESS-KEY", fmt.Sprintf("%s", s.apiCred.ApiKey))
	req.Header.Add("CB-VERSION", "2015-07-22")

	if resp.StatusCode != 200 {
		json.NewDecoder(resp.Body).Decode(&data)
		return fmt.Errorf("coinbase response a bad status code")
	}

	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return err
	}

	return nil
}

func (s Client) GetPrice(pair string) (*types.Price, error) {
	uri := "/v2/prices/" + pair + "/buy"

	var payload struct {
		Data types.Price `json:"data"`
	} = struct {
		Data types.Price `json:"data"`
	}{
		Data: types.Price{},
	}

	if err := s.Get(uri, nil, &payload); err != nil {
		return nil, err
	}

	return &payload.Data, nil
}

func (s Client) GetRates(ratio string) (*types.ExchangeRate, error) {
	uri := "/v2/exchange-rates?currency=" + ratio

	var payload struct {
		Data types.ExchangeRate `json:"data"`
	} = struct {
		Data types.ExchangeRate `json:"data"`
	}{
		Data: types.ExchangeRate{},
	}

	if err := s.Get(uri, nil, &payload); err != nil {
		return nil, err
	}

	return &payload.Data, nil
}
